Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("NTB_FileRoute")> 
<Assembly: AssemblyDescription("Router filer fra mapper til flere ut-mapper")> 
<Assembly: AssemblyCompany("NTB")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("© Norsk Telelgrambyrå AS 2003, Roar Vestre")> 
<Assembly: AssemblyTrademark("® NTB")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("DA89CFBB-7707-4E20-9DE4-C6C2B7BA9182")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.1.*")> 
