Imports System.ServiceProcess
Imports System.IO
Imports System.Xml
Imports System.Configuration
Imports ntb_FuncLib

#If DEBUG Then
Public Class Service1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'Service1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Name = "Service1"
        Me.Text = "Form1"

    End Sub

#End Region

    Private Sub Service1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Init()
    End Sub

    Private Sub Service1_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        StopRoutes()
    End Sub

#End If

#If Not Debug Then
Public Class Service1
    Inherits System.ServiceProcess.ServiceBase

    ' For Release: run as service
    Public Shared Sub Main()
        ServiceBase.Run(New Service1())
    End Sub

    Public Sub New()
        MyBase.New()
        CanStop = True
        CanShutdown = True
        CanPauseAndContinue = False
        ServiceName = "NTB_FileRoute"
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Init()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        StopRoutes()
    End Sub

    Protected Overrides Sub OnShutdown()
        StopRoutes()
    End Sub

#End If

    Private errPath As String = ConfigurationManager.AppSettings("errPath")
    Private logPath As String = ConfigurationManager.AppSettings("logPath")
    Private ftpTempPath As String = ConfigurationManager.AppSettings("FtpTempPath")
    Private xmlFileName As String = ConfigurationManager.AppSettings("xmlFileName")
    Private xmlConfig As New XmlDocument()
    Private htRoutes As New Hashtable()

    Private Sub Init()
        Try
            Directory.CreateDirectory(errPath)
            Directory.CreateDirectory(logPath)
            Directory.CreateDirectory(FtpTempPath)
            xmlConfig.Load(xmlFileName)

            LogFile.WriteLogNoDate(logPath, "----------------------------------------------------------")

            Dim node As XmlNode
            For Each node In xmlConfig.SelectNodes("/route/job")

                Dim id As Integer
                Dim disabled As Boolean = False
                Try
                    id = node.Attributes("id").InnerText
                    disabled = node.Attributes("disabled").InnerText = "true"
                Catch
                End Try

                If Not disabled Then
                    Dim objRoute As New RouteFile(node)
                    objRoute.Start()
                    htRoutes.Add(id, objRoute)
                End If
            Next

            LogFile.WriteLog(logPath, "Programmet er startet og initialisert")
            LogFile.WriteLogNoDate(logPath, "----------------------------------------------------------")
        Catch e As Exception
            LogFile.WriteErr(errPath, "Programmet feilet i Init", e)
            End
        End Try
    End Sub

    Private Sub StopRoutes()
        Dim objRoute As RouteFile

        Try
            For Each objRoute In htRoutes.Values
                objRoute.Pause()
            Next

            For Each objRoute In htRoutes.Values
                objRoute.WaitWhileBusy()
                objRoute = Nothing
            Next

            htRoutes = Nothing

            LogFile.WriteLogNoDate(logPath, "----------------------------------------------------------")
            LogFile.WriteLog(logPath, "Programmet er stoppet")
            LogFile.WriteLogNoDate(logPath, "----------------------------------------------------------")

        Catch e As Exception
            LogFile.WriteErr(errPath, "Programmet feilet i StopRoutes", e)
#If Debug Then
            MsgBox("Programmet feilet i StopRoutes: " & e.Message & e.StackTrace)
#End If
        End Try
    End Sub

End Class
