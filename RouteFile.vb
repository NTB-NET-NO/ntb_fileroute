Imports System.IO
Imports System.Xml
Imports ntb_FuncLib
Imports System.Configuration

Public Class RouteFile

    Private _errPath As String = ConfigurationManager.AppSettings("errPath")
    Private _logPath As String = ConfigurationManager.AppSettings("logPath")
    Private _offLinePath As String = ConfigurationManager.AppSettings("offLinePath")
    Private _node As XmlNode
    Private _fromPath As String
    Private _fileFilter As String = "*.*"
    Private _isBusy As Threading.AutoResetEvent = New Threading.AutoResetEvent(True)
    Private _isActive As Boolean = True
    Private _isOffline As Boolean = True
    Private _mutex As Threading.Mutex = New Threading.Mutex(False, "FILEROUTE_LOGFILE")

    Private timerPoll As Integer = ConfigurationManager.AppSettings("timerPollInterval")
    Private offlinePoll As Integer = ConfigurationManager.AppSettings("offlinePollInterval")
    Private pollWait As Integer = ConfigurationManager.AppSettings("timerFileWait")
    Private retryWait As Integer = ConfigurationManager.AppSettings("retryReadWait")

    'FTP stuff
    Private ftpFetchMode As Boolean = False
    Private ftpGetCfgFile As String
    Private ftpTempPath As String = ConfigurationManager.AppSettings("FtpTempPath")
    Private ftpGetCmd As String = ConfigurationManager.AppSettings("FtpGetCommand")
    Private ftpGetArgs As String = ConfigurationManager.AppSettings("FtpGetArgs")
    Private ftpRecurseArg As String = ConfigurationManager.AppSettings("FtpRecurseArg")
    Private ftpRecurse As Boolean = False

    Private ftpPutCmd As String = ConfigurationManager.AppSettings("FtpPutCommand")
    Private ftpPutArgs As String = ConfigurationManager.AppSettings("FtpPutArgs")

    'Private _nodeList As XmlNodeList
    Friend WithEvents FileSystemWatcher1 As New System.IO.FileSystemWatcher()
    Friend WithEvents Timer1 As New System.Timers.Timer()
    Friend WithEvents TimerOffline As New System.Timers.Timer()

    Public Sub New(ByVal node As XmlNode)
        _node = node
        _fromPath = node.FirstChild.InnerText

        Try
            _fileFilter = _node.SelectSingleNode("inputpath/@filter").InnerText
        Catch
        End Try

        Try
            pollWait = _node.SelectSingleNode("inputpath/@timerFileWait").InnerText
        Catch
        End Try

        Try
            retryWait = _node.SelectSingleNode("inputpath/@retryReadWait").InnerText
        Catch
        End Try

        Try
            ftpFetchMode = (_node.SelectSingleNode("inputpath/@ftp").InnerText = "true")
        Catch
        End Try

        If ftpFetchMode Then

            Dim user As String = ""
            Dim pass As String = ""
            Dim dir As String = New String("")
            Try
                user = _node.SelectSingleNode("inputpath/@username").InnerText
                pass = _node.SelectSingleNode("inputpath/@password").InnerText
                dir = _node.SelectSingleNode("inputpath/@directory").InnerText
                ftpRecurse = (_node.SelectSingleNode("inputpath/@recurse").InnerText = "true")
            Catch
            End Try

            ftpGetCfgFile = ftpTempPath & "\" & _fromPath & "-" & user & "-" & dir.Replace("/", "")
            Dim cfg As String
            cfg = "host " & _fromPath & vbCrLf
            cfg &= "user " & user & vbCrLf
            cfg &= "pass " & pass & vbCrLf
            LogFile.WriteFile(ftpGetCfgFile & ".cfg", cfg, False)

            _fromPath = ftpTempPath & "\" & _fromPath & "-" & user & "-" & dir.Replace("/", "")
        End If

        'Make paths
        Try
            Directory.CreateDirectory(_fromPath)
        Catch ex As Exception
            LogFile.WriteErr(_errPath, "Feil ved oppretting av kilde-path, vil fors�ke p� nytt: ", ex)
        End Try

        MakePaths()

        'Dump some debug stuff
        Dim id As String = _node.Attributes("id").InnerText
        Dim mode As String = "Filepath"
        If ftpFetchMode Then mode = "FTP"

        LogFile.WriteLog(_logPath, "Job " & id & ": Mode: " & mode & " | Source: " & _fromPath & " | Filter: " & _fileFilter & " | Pollwait: " & pollWait)

        FileSystemWatcher1.InternalBufferSize = 8192 * 2
        FileSystemWatcher1.Filter = _fileFilter
        FileSystemWatcher1.NotifyFilter = (NotifyFilters.FileName Or NotifyFilters.LastWrite)
        FileSystemWatcher1.Path = _fromPath
        Start()
    End Sub

    Private Sub MakePaths()

        Dim node As XmlNode
        For Each node In _node.SelectNodes("output/path[@ftp != 'true' or not(@ftp)]")
            Dim toPath As String = node.InnerText
            Try
                Directory.CreateDirectory(toPath)
            Catch e As Exception
                LogFile.WriteErr(_errPath, "Feil ved oppretting av path: ", e)
            End Try
        Next
    End Sub

    Public Sub Start()
        _isActive = True
        TimerOffline.Interval = 60000
        TimerOffline.Enabled = True
        Timer1.Interval = 5000
        Timer1.Enabled = True
    End Sub

    Public Sub Pause()
        _isActive = False
        _isOffline = False
        Timer1.Enabled = False
        TimerOffline.Enabled = False
        FileSystemWatcher1.EnableRaisingEvents = False
    End Sub

    Public Sub WaitWhileBusy()
        Dim i As Integer
        For i = 1 To 15
            If _isBusy.WaitOne(1000, False) Then
                Exit For
            End If
        Next
    End Sub

    Private Sub Timer1_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer1.Elapsed
        ' Timer for asyncron start av servicen
        _isBusy.WaitOne()
        FileSystemWatcher1.EnableRaisingEvents = False
        Timer1.Enabled = False

        Try
            If ftpFetchMode Then
                Dim count As Integer = Directory.GetFiles(_fromPath).GetLength(0)
                If FtpGet() And count > 0 Then
                    LogFile.WriteLog(_logPath, count & " filer hentet via FTP til mappe '" & _fromPath & "'")
                    LogFile.WriteLogNoDate(_logPath, "---------------------------------------------------------------------------------------")
                End If
            End If

            CopyAllFiles(_fromPath, "")
        Catch ex As Exception
            Try
                LogFile.WriteErr(_errPath, "Feil i Timer1_Elapsed: ", ex)
            Catch
            End Try
        End Try

        Timer1.Interval = timerPoll * 1000
        Timer1.Enabled = _isActive
        TimerOffline.Enabled = _isOffline

        Try
            FileSystemWatcher1.EnableRaisingEvents = _isActive And Not ftpFetchMode
        Catch ex As Exception
            Try
                LogFile.WriteErr(_errPath, "Feil ved FileSystemWatcher, vil fors�ke p� nytt: ", ex)
                Directory.CreateDirectory(_fromPath)
            Catch
            End Try
        End Try

        _isBusy.Set()
    End Sub

    Private Sub FileSystemWatcher1_Created(ByVal sender As Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileSystemWatcher1.Created
        _isBusy.WaitOne()
        FileSystemWatcher1.EnableRaisingEvents = False
        Timer1.Enabled = False

        Try
            CopyAllFiles(_fromPath, "")
        Catch ex As Exception
            Try
                LogFile.WriteErr(_errPath, "Feil i FileSystemWatcher1_Created: ", ex)
            Catch
            End Try
        End Try

        Timer1.Enabled = _isActive
        TimerOffline.Enabled = _isOffline

        Try
            FileSystemWatcher1.EnableRaisingEvents = _isActive And Not ftpFetchMode
        Catch ex As Exception
            Try
                LogFile.WriteErr(_errPath, "Feil ved FileSystemWatcher, vil fors�ke p� nytt: ", ex)
                Directory.CreateDirectory(_fromPath)
            Catch
            End Try
        End Try

        _isBusy.Set()
    End Sub

    Private Sub TimerOffline_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles TimerOffline.Elapsed
        _isBusy.WaitOne()
        Timer1.Enabled = False
        TimerOffline.Enabled = False

        Try
            CheckOffline()
        Catch ex As Exception
            Try
                LogFile.WriteErr(_errPath, "Feil i TimerOffline_Elapsed: ", ex)
            Catch
            End Try
        End Try

        TimerOffline.Interval = offlinePoll * 1000
        Timer1.Enabled = _isActive
        TimerOffline.Enabled = _isOffline
        _isBusy.Set()
    End Sub

    Private Function CopyAllFiles(ByVal fromPath As String, ByVal subPath As String) As Boolean
        Dim recurse As Boolean = False
        Dim delete As Boolean = False

        Try
            Dim attrib As String = _node.SelectSingleNode("inputpath/@recurse").InnerText
            recurse = (attrib = "true")
        Catch
        End Try
        Try
            Dim attrib As String = _node.SelectSingleNode("inputpath/@delete").InnerText
            delete = (attrib = "true")
        Catch
        End Try

        If recurse Then
            Dim AllDirs() As String = Directory.GetDirectories(fromPath)
            Dim subDir As String
            Dim orgSubPath As String = subPath
            For Each subDir In AllDirs
                subPath = subPath & subDir.Substring(subDir.LastIndexOf(Path.DirectorySeparatorChar))
                If CopyAllFiles(subDir, subPath) Then
                    If delete Then
                        Try
                            Directory.Delete(subDir)
                        Catch e As Exception
                            LogFile.WriteErr(_errPath, "Feil ved sletting av katalog: ", e)
                        End Try
                    End If
                End If
                subPath = orgSubPath
            Next
        End If

        CopyAllFiles = CheckAndCopyFiles(fromPath, subPath)

    End Function
    Private Function CheckAndCopyFiles(ByVal currentPath As String, ByVal subPath As String) As Boolean
        Dim fileName As String

        CheckAndCopyFiles = True

        If pollWait > 0 Then System.Threading.Thread.Sleep(pollWait * 1000)

        Dim AllFiles() As String = Directory.GetFiles(currentPath, _fileFilter)

        If AllFiles.Length = 0 Then
            Dim node As XmlNode
            For Each node In _node.SelectNodes("output/path")
                Dim toPath As String
                Dim ftpMode As Boolean = False
                Dim rebuildDirectoryStructure As Boolean = False
                Try
                    Dim attrib As String = node.Attributes("ftp").InnerText
                    ftpMode = (attrib = "true")
                Catch
                End Try
                Try
                    Dim attrib As String = node.Attributes("rebuilddirectorystructure").InnerText
                    rebuildDirectoryStructure = (attrib = "true")
                Catch
                End Try
                toPath = node.InnerText

                If rebuildDirectoryStructure Then
                    toPath = toPath & subPath
                    If Not ftpMode Then
                        Try
                            If Not Directory.Exists(toPath) Then
                                Directory.CreateDirectory(toPath)
                            End If
                        Catch e As Exception
                            LogFile.WriteErr(_errPath, "Feil ved oppretting av underkatalog: ", e)
                        End Try
                    End If
                End If
            Next
        End If

        For Each fileName In AllFiles

            Dim ok As Boolean = False

            'Access check in addition to the filewait, 10 tries
            Dim i As Integer = 0
            For i = 0 To 10
                Try
                    Dim str As FileStream
                    str = File.Open(fileName, FileMode.Open, FileAccess.Write, FileShare.None)
                    str.Close()
                    ok = True
                    Exit For
                Catch ex As Exception
                    LogFile.WriteLog(_logPath, "Ingen tilgang til " & fileName & ". Venter " & retryWait & " sek.")
                    LogFile.WriteErr(_logPath, "Ingen tilgang til " & fileName & ". Venter " & retryWait & " sek.", ex)
                    If retryWait > 0 Then
                        System.Threading.Thread.Sleep(retryWait * 1000)
                    Else
                        Exit For
                    End If
                End Try
            Next

            If ok Then
                CopyFile(fileName, currentPath, subPath)
            Else
                CheckAndCopyFiles = False
            End If
        Next

    End Function

    Private Function CopyFile(ByVal fromFile As String, ByVal currentPath As String, ByVal subPath As String) As Boolean
        Dim node As XmlNode
        Dim recurse As Boolean = False
        Dim delete As Boolean = True

        CopyFile = True

        Try
            Dim attrib As String = _node.SelectSingleNode("inputpath/@recurse").InnerText
            recurse = (attrib = "true")
        Catch
        End Try
        Try
            Dim attrib As String = _node.SelectSingleNode("inputpath/@delete").InnerText
            delete = (attrib = "true")
        Catch
        End Try

        _mutex.WaitOne()

        For Each node In _node.SelectNodes("output/path")
            Dim toPath As String
            Dim toFile As String
            Dim datesub As Boolean = False
            Dim disabled As Boolean = False
            Dim ftpMode As Boolean = False
            Dim rebuildDirectoryStructure As Boolean = False

            Try
                Dim attrib As String = node.Attributes("datesub").InnerText
                datesub = (attrib = "true")
            Catch
            End Try

            Try
                Dim attrib As String = node.Attributes("disabled").InnerText
                disabled = (attrib = "true")
            Catch
            End Try

            Try
                Dim attrib As String = node.Attributes("ftp").InnerText
                ftpMode = (attrib = "true")
            Catch
            End Try

            Try
                Dim attrib As String = node.Attributes("rebuilddirectorystructure").InnerText
                rebuildDirectoryStructure = (attrib = "true")
            Catch
            End Try

            toPath = node.InnerText

            If rebuildDirectoryStructure Then
                If Not ftpMode Then
                    toPath = toPath & subPath
                    Try
                        If Not Directory.Exists(toPath) Then
                            Directory.CreateDirectory(toPath)
                        End If
                    Catch e As Exception
                        LogFile.WriteErr(_errPath, "Feil ved oppretting av underkatalog: ", e)
                    End Try
                End If
            End If
            toFile = toPath & Path.DirectorySeparatorChar & Path.GetFileName(fromFile)

            If datesub And Not ftpMode Then
                Dim creationDate As Date
                Try
                    creationDate = File.GetLastWriteTime(fromFile)
                Catch
                End Try

                Try
                    toFile = ntb_FuncLib.FuncLib.MakeSubDirDate(toFile, creationDate)
                Catch ex As Exception
                End Try
            End If

            'Copy file
            If Not disabled Then

                If ftpMode Then
                    'FTP send
                    Dim user As String = ""
                    Dim pass As String = ""
                    Dim dir As String = ""
                    Try
                        user = node.Attributes("username").InnerText
                        pass = node.Attributes("password").InnerText
                        dir = node.Attributes("directory").InnerText
                        If rebuildDirectoryStructure Then
                            dir = dir & subPath.Replace(Path.DirectorySeparatorChar, "/")
                        End If
                    Catch
                    End Try

                    'Send the file
                    Try
                        FtpPut(fromFile, toPath, user, pass, dir)
                        LogFile.WriteLog(_logPath, "Fila er sendt med FTP: " & toFile)
                    Catch ex As Exception
                        LogFile.WriteErr(_errPath, "Feil i FtpPut til " & toPath & ": ", ex)
                        CopyToOffline(fromFile, toPath & "-" & user, True)
                    End Try

                Else
                    'Regular fileshare copy
                    Try
                        File.Copy(fromFile, toFile, True)
                        LogFile.WriteLog(_logPath, "Fila er kopiert: " & toFile)
                    Catch e As Exception
                        LogFile.WriteErr(_errPath, "Feil i  av fil: ", e)
                        CopyToOffline(fromFile, toPath)
                    End Try
                End If
            End If
        Next

        If delete Then
            Try
                File.Delete(fromFile)
                LogFile.WriteLog(_logPath, "InputFila er Slettet: " & fromFile)
            Catch e As Exception
                LogFile.WriteErr(_errPath, "Feil ved sletting av fil: ", e)
                CopyFile = False
            End Try
        End If

        LogFile.WriteLogNoDate(_logPath, "---------------------------------------------------------------------------------------")
        _mutex.ReleaseMutex()
    End Function

    Private Sub CopyToOffline(ByVal fromFile As String, ByVal toPath As String, Optional ByVal ftp As Boolean = False)
        Dim toDir As String = GetOfflinePath(toPath, ftp)
        Directory.CreateDirectory(toDir)
        Dim toFile As String = toDir & "\" & Path.GetFileName(fromFile)
        Try
            System.IO.File.Copy(fromFile, toFile, True)
            LogFile.WriteLog(_logPath, "Fila er kopiert til Offline: " & toFile)
            _isOffline = True
            TimerOffline.Start()
        Catch e As Exception
            LogFile.WriteErr(_errPath, "Feil i kopiering av fil til Offline: ", e)
        End Try
    End Sub

    Private Sub CheckOffline()
        Dim node As XmlNode
        ' Dim nodelist As XmlNodeList
        _isOffline = False

        _mutex.WaitOne()

        'Normal offline files
        For Each node In _node.SelectNodes("output/path[(@ftp != 'true' or not(@ftp)) and (@disabled = 'false' or not(@disabled))]")
            Dim toPath As String = node.InnerText
            Try
                Dim offLinePath As String = GetOfflinePath(toPath)
                If Directory.Exists(offLinePath) Then
                    _isOffline = True
                    CopyFromOffline(offLinePath, node)
                End If
            Catch e As Exception
                'LogFile.WriteErr(_errPath, "Feil i kopiering av fil: ", e)
                LogFile.WriteErr(_errPath, "Feil i kopiering av fil fra OFFLINE: ", e)
            End Try
        Next

        'FTP offline files
        For Each node In _node.SelectNodes("output/path[@ftp = 'true' and (@disabled = 'false' or not(@disabled))]")
            Dim toPath As String = node.InnerText
            Try
                Dim offLinePath As String = GetOfflinePath(toPath & "-" & node.SelectSingleNode("@username").InnerText, True)
                If Directory.Exists(offLinePath) Then
                    _isOffline = True
                    CopyFromOffline(offLinePath, node, True)
                End If
            Catch e As Exception
                'LogFile.WriteErr(_errPath, "Feil i kopiering av fil: ", e)
                LogFile.WriteErr(_errPath, "Feil i FTP sending av fil fra OFFLINE (" & toPath & "): ", e)
            End Try
        Next

        _mutex.ReleaseMutex()
    End Sub

    Private Sub CopyFromOffline(ByVal offLinePath As String, ByVal node As XmlNode, Optional ByVal ftp As Boolean = False)
        Dim toPath As String = node.InnerText
        Dim deleteOk As Boolean = True
        Dim fileName As String

        Dim AllFiles() As String = Directory.GetFiles(offLinePath)
        System.Threading.Thread.Sleep(3000)

        For Each fileName In AllFiles
            If ftp Then
                'FTP send
                Dim user As String = ""
                Dim pass As String = ""
                Dim dir As String = ""

                Try
                    user = node.Attributes("username").InnerText
                    pass = node.Attributes("password").InnerText
                    dir = node.Attributes("directory").InnerText
                Catch
                End Try

                'Send the file
                'Try

                FtpPut(fileName, toPath, user, pass, dir)
                LogFile.WriteLog(_logPath, "Fila er sendt med FTP fra OFFLINE (" & toPath & "): " & fileName)
                File.Delete(fileName)

                'Catch ex As Exception
                '    deleteOk = False
                '    LogFile.WriteErr(_errPath, "Feil i FTP sending av fil fra OFFLINE (" & toPath & "): ", ex)
                'End Try
            Else
                'Regular copy
                Dim toFile As String = toPath & "\" & Path.GetFileName(fileName)

                'Try

                File.Copy(fileName, toFile, True)
                LogFile.WriteLog(_logPath, "Fila er kopiert fra OFFLINE: " & toFile)
                File.Delete(fileName)

                'Catch e As Exception
                '    deleteOk = False
                '    LogFile.WriteErr(_errPath, "Feil i kopiering av fil fra OFFLINE: ", e)
                'End Try

            End If

        Next

        If AllFiles.GetLength(0) > 0 Then LogFile.WriteLogNoDate(_logPath, "---------------------------------------------------------------------------------------")

        If deleteOk Then
            Directory.Delete(offLinePath)
        End If

    End Sub

    Private Function GetOfflinePath(ByVal toPath As String, Optional ByVal ftp As Boolean = False) As String
        toPath = toPath.Replace(":", "-disk")
        toPath = toPath.Replace("\\", "server-")
        If ftp Then toPath = "ftp-" & toPath
        Return _offLinePath & "\" & toPath
    End Function

    'Sends a file via ncftpput
    Private Function FtpPut(ByVal strFileName As String, ByVal server As String, ByVal username As String, ByVal password As String, ByVal folder As String) As Boolean

        Dim retval As Integer
        Dim jobfilefolder As String = folder

        If (Not folder Is Nothing) Then
            If (folder.Length > 0) Then
                If (folder.IndexOf("/") >= 0) Then
                    jobfilefolder = folder.Substring(0, folder.IndexOf("/"))
                End If
            End If
        End If
        Dim jobfile As String = ftpTempPath & "\" & server & "-" & username & "-" & jobfilefolder
        Dim cfg As String
        cfg = "host " & server & vbCrLf
        cfg &= "user " & username & vbCrLf
        cfg &= "pass " & password & vbCrLf
        LogFile.WriteFile(jobfile & ".cfg", cfg, False)

        If folder = "" Then folder = "."

        Dim st As ProcessStartInfo = New ProcessStartInfo
        Dim p As Process

        st.FileName = ftpPutCmd
        Dim ftpPutLocalArgs As String = ftpPutArgs
        If (False) Then
            ftpPutLocalArgs = ftpPutLocalArgs & " " & ftpRecurseArg
        End If
        st.Arguments = "-f """ & jobfile & ".cfg""" & " -d """ & jobfile & ".log""" & " -e """ & jobfile & ".err"" " & ftpPutLocalArgs & " " & folder & " """ & strFileName & """"
        st.UseShellExecute = False
        st.CreateNoWindow = True
        st.WindowStyle = ProcessWindowStyle.Hidden

        p = System.Diagnostics.Process.Start(st)

        p.WaitForExit()
        retval = p.ExitCode
        p.Close()

        If retval > 0 Then
            Dim ex As Exception = New Exception("NCFtpPut Error return value: " & retval)
            Throw ex
        End If

        Try
            File.Delete(jobfile & ".log")
            'File.Delete(jobfile & ".err")
        Catch
        End Try

        Return True

    End Function

    'Fetches files via ncftpget
    Private Function FtpGet() As Boolean

        Dim retval As Integer = 0
        Dim folder As String = ""
        Dim ret As Boolean = False

        Try
            folder = _node.SelectSingleNode("inputpath/@directory").InnerText
        Catch
        End Try

        If folder <> "" Then folder &= "/"

        Dim st As ProcessStartInfo = New ProcessStartInfo
        Dim p As Process

        st.FileName = ftpGetCmd
        Dim ftpGetLocalArgs As String = ftpGetArgs
        If (ftpRecurse) Then
            ftpGetLocalArgs = ftpGetLocalArgs & " " & ftpRecurseArg
        End If
        st.Arguments = "-f """ & ftpGetCfgFile & ".cfg""" & " -d """ & ftpGetCfgFile & ".log""" & " -e """ & ftpGetCfgFile & ".err"" " & ftpGetLocalArgs & " " & _fromPath & " " & folder & _fileFilter
        st.UseShellExecute = False
        st.CreateNoWindow = True
        st.WindowStyle = ProcessWindowStyle.Hidden

        p = System.Diagnostics.Process.Start(st)

        p.WaitForExit()
        retval = p.ExitCode
        p.Close()

        Dim f As FileInfo = New FileInfo(ftpGetCfgFile & ".err")

        If retval = 0 Then
            ret = True
        ElseIf f.Exists Then
            If f.Length = 0 Then ret = True
        ElseIf Not f.Exists Then
            ret = True
        Else
            ret = False
        End If

        If ret Then
            File.Delete(ftpGetCfgFile & ".log")
            File.Delete(ftpGetCfgFile & ".err")
        End If

        Return ret
    End Function

End Class
